# Sem Chamada

Exercício da Pluri Educacional.

## Instruções

`npm install` para instalar as dependências.

### Develop
`npm run dev` roda o servidor webpack de desenvolvimento com auto refresh.

### Production
`npm run build` compila o projeto.
`npm start` inicia o servidor node.
