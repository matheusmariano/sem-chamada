import angular from 'angular';

angular
  .module('SemChamada')
  .constant('consts', {
    KNOWLEDGE_COLORS: {
      '1': 'green',
      '2': 'amber',
      '3': 'blue',
      '4': 'red',
    },
  });
