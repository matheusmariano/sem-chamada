import angular from 'angular';

angular
  .module('SemChamada')
  .config(routes);

routes.$inject = ['$locationProvider', '$stateProvider'];

function routes($locationProvider, $stateProvider) {
  $locationProvider.html5Mode(true);

  $stateProvider
    .state('login', {
      name: 'Login',
      url: '/',
      template: require('../../views/login/index.html'),
      controller: 'LoginController',
      resolve: {
        auth: ['authFactory', '$state', '$q', (authFactory, $state, $q) => {
          if (authFactory.get()) {
            $state.go('lessons');
            return $q.reject();
          } else {
            return $q.when();
          }
        }],
      },
    })
    .state('lessons', {
      name: 'Aulas',
      url: '/aulas',
      template: require('../../views/lessons/index.html'),
      controller: 'LessonsController',
      resolve: {
        auth: ['authFactory', '$state', '$q', (authFactory, $state, $q) => {
          if (authFactory.get()) {
            return $q.when();
          } else {
            $state.go('login');
            return $q.reject();
          }
        }],
      }
    });
}
