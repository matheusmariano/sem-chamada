import angular from 'angular';

angular
  .module('SemChamada')
  .config(theming);

theming.$inject = ['$mdThemingProvider'];

function theming($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('green')
    .accentPalette('blue');
}
