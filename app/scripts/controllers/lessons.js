import angular from 'angular';
import R from 'ramda';
import diacritics from 'diacritics';

angular
  .module('SemChamada')
  .controller('LessonsController', LessonsController);

LessonsController.$inject = ['$scope', 'consts', 'lessonFactory'];

function LessonsController($scope, consts, lessonFactory) {
  $scope.clearFilter = () => {
    $scope.search = '';
  };

  $scope.diacriticsFilter = (item) => {
    if (!$scope.search) {
      return true;
    }

    const search = clearString($scope.search);

    return R.pipe(
      R.values,
      R.map(clearString),
      R.any(
        R.contains(search)
      ),
    )(item);
  };

  $scope.getColor = (id) => {
    return consts.KNOWLEDGE_COLORS[id];
  };

  $scope.lessons = [];

  $scope.loading = false;

  getLessons();

  function getLessons() {
    $scope.loading = true;
    lessonFactory.all().then(data => {
      $scope.lessons = data;
      $scope.loading = false;
    });
  }
}

function clearString(string) {
  return R.pipe(
    R.toLower,
    diacritics.remove,
  )(string);
};
