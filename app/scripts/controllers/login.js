import angular from 'angular';

angular
  .module('SemChamada')
  .controller('LoginController', LoginController);

LoginController.$inject = ['$scope', '$state', 'loginFactory'];

function LoginController($scope, $state, loginFactory) {
  $scope.attemptError = null;
  $scope.form = {};
  $scope.loading = false;

  $scope.attemptLogin = function (form) {
    $scope.attemptError = null;
    $scope.loading = true;

    loginFactory
      .attempt(form.email, form.senha)
      .then(data => {
        $scope.loading = false;

        if (data.sucesso === true) {
          $state.go('lessons');
        } else {
          $scope.attemptError = data.erros[0].descricaoErro;
        }
      });
  };
}
