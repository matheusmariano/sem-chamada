import angular from 'angular';

angular
  .module('SemChamada')
  .factory('authFactory', authFactory);

authFactory.$inject = ['localStorageService'];

function authFactory(storage) {
  return {
    get: () => {
      return storage.get('userToken');
    },
    save: token => {
      storage.set('userToken', token);
    }
  };
}
