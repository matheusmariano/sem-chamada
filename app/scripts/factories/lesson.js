import angular from 'angular';
import R from 'ramda';

angular
  .module('SemChamada')
  .factory('lessonFactory', lessonFactory);

lessonFactory.$inject = ['$http', 'localStorageService'];

function lessonFactory($http, storage) {
  return {
    all: () => {
      return $http
        .get('http://homologacao-api.semchamada.com.br/aulas', {
          headers: {
            'Authorization': storage.get('userToken'),
          }
        })
        .then(({ data }) => {
          return data;
        });
    },
  };
}
