import angular from 'angular';
import R from 'ramda';

angular
  .module('SemChamada')
  .factory('loginFactory', loginFactory);

loginFactory.$inject = ['$http', 'authFactory'];

function loginFactory($http, authFactory) {
  return {
    attempt: (login, senha) => {
      return $http.post('http://plurieducacional.com.br/homologacao/pluriidapi/webservice.php', {
        operacao: '008',
        chaveSistema: '1b6453892473a467d07372d45eb05abc2031647a',
        login: login,
        senha: senha,
      }).then(response => {
        const { data } = response;

        if (data.sucesso === true) {
          authFactory.save(
            R.path(['informacoesUsuario', 'token'], data),
          );
        }

        return data;
      });
    }
  };
}
