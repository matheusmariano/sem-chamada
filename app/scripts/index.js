import angular from 'angular';
import 'angular-local-storage';
import 'angular-material';
import 'angular-material/angular-material.scss';
import 'angular-messages';
import 'angular-ui-router';
import '../styles/main.scss';

angular
  .module('SemChamada', [
    'LocalStorageModule',
    'ngAnimate',
    'ngAria',
    'ngMaterial',
    'ngMessages',
    'ui.router',
  ]);

/**
 * Controllers
 */
require('./controllers/lessons');
require('./controllers/login');

/**
 * Factories
 */
require('./factories/auth');
require('./factories/lesson');
require('./factories/login');

/**
 * Configs
 */
require('./config/consts');
require('./config/routes');
require('./config/theming');
