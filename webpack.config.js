var webpack = require('webpack')
var path = require('path')
var R = require('ramda')

var UglifyJsPlugin = webpack.optimize.UglifyJsPlugin

module.exports = {
  // Main file, where your project starts.
  entry: './app/scripts/index.js',

  // Output file, where your app should be compiled and imported by index.html.
  output: {
    path: path.join(__dirname, "public"),
    filename: 'bundle.js'
  },

  // Compiling parameters:
  module: {
    rules: [
      {
        // all the files finished with .js or .jsx
        test: /\.js$/,

        // except the node_modules folder
        exclude: /node_modules/,

        // should be converted by Babel
        loader: 'babel-loader',

        // using ES2015 and React presets
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          minimize: true
        }
      },
      {
        test: /\.s?css$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(jpg|png)$/,
        loader: 'file-loader'
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.html', '.scss', '.css', '.jpg', '.png']
  },

  // Development server parameters:
  devServer: {
    inline: true,

    // Root folder
    contentBase: './public',

    // Port
    port: 8000,

    // Live Reload <3 Routes
    historyApiFallback: true
  },

  devtool: 'source-map',

  plugins: R.concat(
    // Any environment plugins.
    [
      // Add environment variables.
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
      }),
    ],

    // Specific environment plugins.
    R.cond([
      // Production plugins.
      [R.equals('production'), R.always([
        new UglifyJsPlugin()
      ])],

      // Other environments.
      [R.T, R.always([])]
    ])(process.env.NODE_ENV)
  )
}
